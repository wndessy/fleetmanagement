

<%@page import="java.io.IOException"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.File"%>
<!--%@page contentType="application/octet-stream" pageEncoding="UTF-8"%-->



<!DOCTYPE html>

<html>
    <head>
        <title>Rickshaw travels  limited</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">

        <link rel="stylesheet"  type="text/css" href="../frameworks/bootstrap-3.3.4-dist/css/bootstrap.min.css"/>
        <link rel="stylesheet"  type="text/css" href="../frameworks/bootstrap-3.3.4-dist/css/bootstrap-theme.min.css"/>
        <link rel="stylesheet"  type="text/css" href="../frameworks/bootstrap-3.3.4-dist/css/bootstrap-theme.css.map"/>
        <link rel="stylesheet"  type="text/css" href="../frameworks/bootstrap-3.3.4-dist/css/bootstrap-theme.min.css"/>
        <link href="../plugins/select2-4.0.0/dist/css/select2.min.css" type="text/css" rel="stylesheet" />

        <link rel="stylesheet"  type="text/css" href="../plugins/datepicker/bootstrap/dist/css/bootstrap.min.css" />
        <link rel="stylesheet"  type="text/css" href="../plugins/datepicker/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />


        <link rel="stylesheet"  type="text/css" href="../frameworks/bootstrap-3.3.4-dist/css/bootstrap.min.css"/>
        <link rel="stylesheet"  type="text/css" href="../plugins/DataTables-1.10.6/media/css/jquery.dataTables_themeroller.css"/>
        <link rel="stylesheet"  type="text/css" href="../plugins/DataTables-1.10.6/media/css/jquery.dataTables.min.css"/>
        <link rel="stylesheet"  type="text/css" href="../plugins/sweetalert-master/dist/sweetalert.css"/>
    </head>
    <body>
        <div class="container">
            <div class="row text-right">
                <button type="button" class="btn btn-primary" id="downloadReport">Print</button>
            </div>
            <div class="row" id="header">
                <div class="col-md-2">
                    <img src="../assets/images/logo-cop.png" class="img-responsive" alt="Responsive image">
                </div>	
                <div class="col-md-6">
                    <h2>RICKSHOW TRANSPORT SYSTEM</h2>
                </div>
            </div>
            <div class="row">
                <table id="report-table" class="compact" cellspacing="0" width="95%">
                    <thead>
                        <tr>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>  
            </div>

        </div>

        <script type="text/javascript" src="../frameworks/jquery-2.1.3.min.js"></script>
        <script type="text/javascript" src="../frameworks/bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../plugins/datepicker/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../plugins/DataTables-1.10.6/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../plugins/sweetalert-master/dist/sweetalert.min.js"></script>

        <script>
            $(document).ready(function() {
                var url = window.location.href;
                var data = {};
                var val1 = url.split("?")[1].split("&");
                $.each(val1, function(i, val) {
                    var parameters = val.split("=");
                    data[parameters[0]] = parameters[1]; // Add each to features object
                });

                $.post("../ReportController",
                        {
                            item: data.report,
                            action: "preview-report",
                            data: JSON.stringify(data),
                        },
                        function(data) {
                            var table = $('#report-table').dataTable({
                                paging: false,
                                retrieve: true,
                                searching: false,
                                ordering: false,
                                info: false,
                                data: data,
                                "columns": [
                                    {"data": "pickuptime", title: "pick up time"},
                                    {"data": "clientsName", title: "Client Name"},
                                    {"data": "pickpPlace", title: "From"},
                                    {"data": "dropOffPlace", title: "To"},
                                    {"data": "payment", title: "Payment Ksh"},
                                ]
                            });

                        }).fail(function(error) {
                    alert("error");
                    console.log(error);
                });

                function printReport() {
                    var data1 = {item: "Report",
                        type: "Report",
                        action: "print_report",
                        data: JSON.stringify({url: window.location.href})
                    };
                    $.download("../ReportController", data1);
                }
                jQuery.download = function(url, data, method) {
                    //url and data options required
                    if (url && data) {
                        //data can be string of parameters or array/object
                        data = typeof data == 'string' ? data : jQuery.param(data);
                        //split params into form inputs
                        var inputs = '';
                        jQuery.each(data.split('&'), function() {
                            var pair = this.split('=');
                            inputs += '<input type="hidden" name="' + pair[0] + '" value="' + pair[1] + '" />';
                        });
                        //send request
                        jQuery('<form accept-charset=utf-8 action="' + url + '" method="' + (method || 'post') + '">' + inputs + '</form>').appendTo('body').submit().remove();
                    }

                };

                $(document).on("click", "#downloadReport", function() {
                    printReport();
                });
            });
        </script>


    </body>

</html>
