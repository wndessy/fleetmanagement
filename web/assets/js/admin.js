//datepicker 

(function() {
    this.User = (function() {
        User = function() {
        };
        User.delteallert = function(element, item, attribute) {
            sweetAlert({
                title: "Are you sure of delete?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "red",
                confirmButtonText: "Yes, delete!",
                cancelButtonColor: "green",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("../EntityController", {item: item, action: "delete", data: $(element).attr(attribute)}, function(data) {
                        if (data === "logout") {
                            window.location.href = "lockPage.html";
                        } else {
                            sweetAlert({
                                title: "Sucess",
                                text: data,
                                type: "success",
                                timer: 1000,
                                showConfirmButton: false},
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location.reload();
                                }
                            });
                        }
                    });
                } else {
                    sweetAlert({
                        title: "Cancelled",
                        text: "Delete action Cancelled ",
                        type: "error",
                        timer: 1000,
                        showConfirmButton: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            window.location.reload();
                        }
                    });
                }

            });
        }


        User.savealert = function(item, data) {
            $.post("../EntityController", {item: item, action: "add", data: JSON.stringify(data)}, function(response) {
                if (response === "logout") {
                    window.location.href = "lockPage.html";
                } else {
                    swal({
                        title: "Success!",
                        text: "New " + item + " added successfully",
                        type: "success",
                        // timer: 1000,
                        showConfirmButton: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            if (item === "allocatevehicle") {
                                window.location.reload();
                                //     window.location.reload();
                                var win = window.open('../html/voucher.html?data=' + JSON.stringify(data), '_blank');
                                if (win) {
                                    //Browser has allowed it to be opened
                                    win.focus();
                                } else {
                                    //Broswer has blocked it
                                    alert('Please allow popups for this site');
                                }
                            } else {
                                //                              window.location.reload();
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        };





        /**
         * Datatables
         */
        //client
        $.post("../EntityController", {item: "Client", action: "select", type: "Client"}, function(data) {
            if (data === "logout") {
                window.location.href = "lockPage.html";
            } else {
                //console.log(data)
                function format(d) {
                    // `d` is the original data object for the row
                    var childTabe = '<table id ="table' + d + '" class="table-condensed subtable" cellspacing="0" border="0" ">' +
                            '<tbody></tbody>' +
                            '</table>';
                    return childTabe;
                }

                var clienttable = $('#client_table').DataTable({
                    data: data,
                    "columns": [
                        {"data": "userEnityId", title: "", visible: false},
                        {"className": 'details-control', "orderable": false, "data": null, "defaultContent": ''},
                        {"data": "clientType", "title": "Client Type"},
                        {"data": "name", title: "Name"},
                        {"data": "email", title: "Email", "orderable": false},
                        {"data": "phone", title: "Phone Number", "orderable": false, },
                        {"data": "status", title: "Status", },
                        {"data": "location", title: "Location"},
                        {"data": "cancel", title: "", "orderable": false, }
                    ],
                });

                // Add event listener for opening and closing details
                $('#client_table tbody').on('click', 'td', function() {
                    var tr = $(this).closest('tr');
                    var row = clienttable.row(tr);
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child(format(row.data().userEnityId)).show();
                        tr.addClass('shown');
                        $.post("../EntityController", {item: "DriverAssignment", oneOfManyClientId: row.data().userEnityId, type: "specificDriver"}, function(dataResponse) {
                            if (dataResponse === "logout") {
                                window.location.href = "lockPage.html";
                            } else {
                                console.log(dataResponse);
                                if (dataResponse.length !== 0) {
                                    var subtable = $("#table" + row.data().userEnityId).DataTable({
                                        paging: false,
                                        "ordering": false,
                                        "scrollX": false,
                                        responsive: true,
                                        "searching": false,
                                        "info": false,
                                        scroly: true,
                                        data: dataResponse,
                                        "columns": [
                                            {"data": "pickupTime", title: "pickupTime"},
                                            {"data": "pickupPlace", title: "pickupPlace"},
                                            {"data": "dropOffPlace", title: "dropOffPlace"},
                                            {"data": "clientName", title: "clientName"},
                                            {"data": "driverName", title: "driverName"},
                                            {"data": "regNo", title: "vehicle   "}
                                        ]
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });







        //vehicle
        $.post("../EntityController", {item: "Vehicle", action: "select"}, function(data) {
            if (data === "logout") {
                window.location.href = "lockPage.html";
            } else {
                //console.log(data)
                function format(d) {
                    // `d` is the original data object for the row
                    var childTabe = '<div class="subtabe"><table id ="table' + d + '" class="table-condensed subtable">' +
                            '<tbody></tbody>' +
                            '</table></div>';
                    return childTabe;
                }



                var table = $('#vehicle_table').DataTable({
                    //responsive: true,
                    "data": data,
                    "columns": [
                        {"data": "vehicleId", title: "", visible: false},
                        {"className": 'details-control', "orderable": false, "data": null, "defaultContent": ''},
                        {"data": "regNo", "title": "Registration "},
                        {"data": "model", title: "Model"},
                        {"data": "color", title: "Color"},
                        {"data": "engine", title: "Engine capacity"},
                        {"data": "features", title: "Other featues"},
                        {"data": "assigned", title: "Status"},
                        {"data": "cancel", title: ""}
                    ],
                });
                // Add event listener for opening and closing details
                $('#vehicle_table tbody').on('click', 'td', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child(format(row.data().vehicleId)).show();
                        tr.addClass('shown');
                        $.post("../EntityController", {item: "DriverAssignment", oneOfManyVehicleId: row.data().vehicleId, type: ""}, function(dataResponse) {
                            if (dataResponse === "logout") {
                                window.location.href = "lockPage.html";
                            } else {


                                if (dataResponse.length !== 0) {
                                    var subtable = $("#table" + row.data().vehicleId).DataTable({
                                        "paging": false,
                                        "ordering": false,
                                        "scrollX": false,
                                        "responsive": true,
                                        "searching": false,
                                        "info": false,
                                        "scrolY": true,
                                        "data": dataResponse,
                                        "columns": [
                                            {"data": "pickupTime", title: "pickupTime"},
                                            {"data": "pickupPlace", title: "pickupPlace"},
                                            {"data": "dropOffPlace", title: "dropOffPlace"},
                                            {"data": "clientName", title: "clientName"},
                                            {"data": "regNo", title: "vehicle"}
                                        ]
                                    });
                                }
                            }
                        });

                    }
                });

            }
        }).fail(function() {
            cnsole.log("error");
        });







//driver
        $.post("../EntityController", {item: "Driver", action: "select", type: "Client"}, function(data) {
            if (data === "logout") {
                window.location.href = "lockPage.html";
            } else {
                //console.log(data)
                function format(d) {
                    // `d` is the original data object for the row
                    var childTable = '<table id ="table'+ d +'"class="table-condensed subtable" ">' +
                            '<tbody></tbody>' +
                            '</table>';
                    return childTable;
                }
                var table = $('#driver_table').DataTable({
                    "data": data,
                    "columns": [
                        {"data": "userEnityId", title: "", visible: false},
                        {"className": 'details-control', "orderable": false, "data": null, "defaultContent": ''},
                        {"data": "name", title: "Name"},
                        {"data": "idNo", title: "Id or Passport"},
                        {"data": "email", title: "Email"},
                        {"data": "phone", title: "Phone Number"},
                        {"data": "cancel", title: ""}
                    ],
                    "fnCreatedRow": function(nRow, aData, iDataIndex) {
                        $(nRow).attr('myidentity', iDataIndex);
                    }
                });
                // Add event listener for opening and closing details
                $('#driver_table tbody').on('click', 'td', function() {
                    var tr = $(this).closest('tr');
                    var row = table.row(tr);
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    }
                    else {
                        // Open this row
                        row.child(format(row.data().userEnityId)).show();
                        tr.addClass('shown');
                        $.post("../EntityController", {item: "DriverAssignment", oneOfManyDriverId: row.data().userEnityId, type: "specificDriver"}, function(dataResponse) {
                            if (dataResponse === "logout") {
                                window.location.href = "lockPage.html";
                            } else {
                                if (dataResponse.length !== 0) {
                                    var subtable = $("#table"+row.data().userEnityId).DataTable({
                                        paging: false,
                                        "ordering": false,
                                        "scrollX": false,
                                        responsive: true,
                                        "searching": false,
                                        "info": false,
                                        scroly: true,
                                        data: dataResponse,
                                        "columns": [
                                            {"data": "pickupTime", title: "pickupTime"},
                                            {"data": "pickupPlace", title: "pickupPlace"},
                                            {"data": "dropOffPlace", title: "dropOffPlace"},
                                            {"data": "clientName", title: "clientName"},
                                            {"data": "regNo", title: "vehicle   "}
                                        ]
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });













//agent
        $.post("../EntityController", {item: "Agent", action: "select", type: "Agent"}, function(data) {
            if (data === "logout") {
                window.location.href = "lockPage.html";
            } else {
                $('#agent_table').DataTable({
                    "dom": 'Rlfrtip',
                    data: data,
                    "columns": [
                        {"data": "name", title: "Name"},
                        {"data": "IdNo", title: "Id or Passport"},
                        {"data": "email", title: "Email"},
                        {"data": "phone", title: "Phone Number"},
                        {"data": "cancel", title: ""}
                    ]
                });
            }
        });




        //onload
        /**
         * 
         * reports
         */
        var datepickerReportStart = $('#reportStartDate').datetimepicker({
            showClear: true,
            maxDate: "now",
            format: 'DD-MM-YYYY',
            toolbarPlacement: "top"
        });
        var datepickerReportEnd = $('#reportEndDate').datetimepicker({
            showClear: true,
            maxDate: "now",
            format: 'DD-MM-YYYY',
            toolbarPlacement: "top"
        });
        /**
         * /load add new modals
         * 
         */
        User.ButtonClicked = function(element) {

            if ($(element).attr("name") === "newClientModalbtn") {
                var newClientModal = $("#clientModal").modal({
                    backdrop: 'static',
                    keyboard: false

                });
            }
            if ($(element).attr("name") === "newdriverModalbtn") {
                var newDriverModal = $("#driverModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            }
            if ($(element).attr("name") === "newVehicleModalBtn") {
                var newVehicleModal = $("#vehicleModal").modal({
                    backdrop: 'static',
                    keyboard: false

                });
            }
            if ($(element).attr("name") === "newAgentModalbtn") {
                var newAgentModal = $("#agentModal").modal({
                    backdrop: 'static',
                    keyboard: false

                });
            }
            /**
             * report
             */







            if ($(element).attr("name") === "prevew-report") {
                var data = {};
                $("#reportForm").not("button").find("input,select").each(function() {
                    data[$(this).attr('name')] = $(this).val(); // Add each to features object
                });
                $.post("../ReportController",
                        {
                            item: $("select[name='report']").val(),
                            action: "preview-report",
                            data: JSON.stringify(data),
                            datatype: "json",
                        },
                        function(data) {
                            if (data === "logout") {
                                window.location.href = "lockPage.html";
                            } else {
                                var myarray = [
                                    {"data": "payment", title: "Payment Ksh"},
                                    {"data": "pickuptime", title: "pick up time", visible: true},
                                    {"data": "clientsName", title: "Client Name"},
                                    {"data": "pickpPlace", title: "From"},
                                    {"data": "dropOffPlace", title: "To"},
                                    {"data": "allocationStatus", title: "alloction Status"},
                                    //clients
                                    {"data": "phone", title: " Client Phone", visible: false, "paging": false},
                                    {"data": "clientStatus", title: " Client Status", visible: false, "paging": false},
                                    //vehicle
                                    {"data": "vehicleRegNo", title: "vehicle Reg", visible: false},
                                    {"data": "vehicleModel", title: " vehicle Model", visible: false, "paging": false},
                                    {"data": "vehicleStatus", title: " vehicle Status", visible: false, "paging": false},
                                    //driver
                                    {"data": "driverName", title: "Driver Name", visible: false},
                                    {"data": "driverPhone", title: "Driver Phone", visible: false, "paging": false},
                                    {"data": "driverStatus", title: "Driver Status", visible: false, "paging": false},
                                    //agent
                                    {"data": "agentName", title: "Agent Name", visible: false},
                                    {"data": "agentPhone", title: "Agent Phone", visible: false, "paging": false},
                                    {"data": "agentStatus", title: " Agent Status", visible: false, "paging": false},
                                    {"data": "payment", title: "Payment Ksh"},
                                ];

                                var table = $('#report-table').DataTable({
                                    retrieve: true,
                                    dom: 'CT<"clear">lfrtip',
                                    data: data,
                                    paging: true,
                                    "columns": myarray,
                                    tableTools: {
                                        "sSwfPath": "../plugins/DataTables-1.10.6/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                                        "sRowSelect": "single",
                                    },
                                    "footerCallback": function() {
                                        var api = this.api(), data;
                                        // Remove the formatting to get integer data for summation
                                        var intVal = function(i) {
                                            return typeof i === 'string' ?
                                                    i.replace(/[\$,]/g, '') * 1 :
                                                    typeof i === 'number' ?
                                                    i : 0;
                                        };
                                        // Total over all pages
                                        total = api
                                                .column(0)
                                                .data()
                                                .reduce(function(a, b) {
                                                    return intVal(a) + intVal(b);
                                                });
                                        // Total over this page
                                        pageTotal = api.column(4, {page: 'current'}).data()
                                                .reduce(function(a, b) {
                                                    return intVal(a) + intVal(b);
                                                }, 0);
                                        // Update footer
                                        $(api.column(0).footer()).html(
                                                ' Total : Ksh' + total 
                                                );
                                    },
                                });
                            }
                        });
            }

















            /**
             * assign vehicle modal load
             */
            if ($(element).attr("name") === "assignVehicleBtn") {
                var assignModal = $("#assignVehicleModal").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                //control assign vehicle modal
                assignModal.on('shown.bs.modal', function() {
                    $.post("../EntityController", {item: "selectallocatedVehiclePerClient", data: $(element).attr("data-id")}, function(data) {
                        if (data === "logout") {
                            window.location.href = "lockPage.html";
                        } else {
                            if (data.length !== 0) {
                                var table = $('#client-allocation-table').DataTable({
                                    "dom": 'Rlfrtip',
                                    paging: false,
                                    retrieve: true,
                                    searching: false,
                                    ordering: false,
                                    data: data,
                                    "columns": [
                                        {"data": "vehicle", "title": "Vehicle "},
                                        {"data": "pickuptime", title: "Pick up time"},
                                        {"data": "pickupplace", title: "Pick up place"},
                                        {"data": "dropOffplace", title: " Drop off place"},
                                        {"data": "driver", title: "Driver"},
                                        {"data": "cancel", title: ""},
                                    ]
                                });
                            }
                        }
                    });
                    $("[name=alocateVehicleBtn]").attr("value", "" + $(element).attr("data-id") + "");
                    $.post("../EntityController", {item: "Driver", action: "select"}, function(data) {
                        if (data === "logout") {
                            window.location.href = "lockPage.html";
                        } else {
                            var drivers = "";
                            $.each(data, function(i, item) {
                                drivers += "<option value='" + item.userEnityId + "'>" + item.name + "</option>";
                            });
                            $("#driver-select").html(drivers);
                        }
                    });
                    $.post("../EntityController",
                            {item: "Vehicle", action: "select"}, function(data) {
                        if (data === "logout") {
                            window.location.href = "lockPage.html";
                        } else {
                            var veicles = "";
                            $.each(data, function(i, item) {
                                veicles += ("<option value='" + item.vehicleId + "'>" + item.regNo + " " + item.model + " " + "</option>");
                            });
                            $("#vehicle-select").html(veicles);
                        }
                    });
                    // $('select').select2();
                    assignModal.on('hide.bs.modal', function() {
                    });
                    var datepicker = $('#datetimepicker7').datetimepicker({
                        sideBySide: true,
                        showTodayButton: true,
                        showClear: true,
                        showClose: true,
                        minDate: "now",
                        toolbarPlacement: "top"
                    });
                });
            }


            if ($(element).attr("name") === "cancelVehicleAllocationBtn") {
                this.delteallert($(element), "cancelVehicleAllocation", "id")
            }
            if ($(element).attr("name") === "deleteVehicleBtn") {
                this.delteallert($(element), "Vehicle", "data-id")
            }
            if ($(element).attr("name") === "deleteDriverBtn") {
                this.delteallert($(element), "Driver", "data-id");
            }
            if ($(element).attr("name") === "deleteClientBtn") {
                this.delteallert($(element), "Client", "data-id");
            }
            if ($(element).attr("name") === "deleteAgentBtn") {
                this.delteallert($(element), "Agent", "data-id");
            }
        };
        /**
         * forms
         */

        $('#client_form').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
//alert("issues");
            } else {
                var data = {};
                $("#client_form").not("button").find("input,select").each(function() {
                    data[$(this).attr('name')] = $(this).val(); // Add each to features object
                });
                User.savealert("Client", data);
                e.preventDefault();
            }
        });
        $('#driver-form').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
//alert("issues");
            } else {
                var data = {};
                $("#driver-form").not("button").find("input,select").each(function() {
                    data[$(this).attr('name')] = $(this).val(); // Add each to features object
                });
                User.savealert("Driver", data)
                e.preventDefault();
            }
        });
        $('#vehicle-form').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
//alert("issues");
            } else {
                var data = {};
                $("#vehicle-form").not("button").find("input,select").each(function() {
                    data[$(this).attr('name')] = $(this).val(); // Add each to features object
                });
                User.savealert("Vehicle", data)
                e.preventDefault();
            }
        });
        $('#agent-form').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
//alert("issues");
            } else {
                var data = {};
                $("#agent-form").not("button").find("input,select").each(function() {
                    data[$(this).attr('name')] = $(this).val(); // Add each to features object
                });
                User.savealert("Agent", data)
                e.preventDefault();
            }
        });
        $('#asign-Vehicl-Form').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
            } else {
                e.preventDefault();
                //alert("hellow")
                var data = {};
                data["userId"] = $("button[name=alocateVehicleBtn]").attr("value")
                $("#asign-Vehicl-Form").find("input,select,button[name=alocateVehicleBtn]").each(function() {
                    data[$(this).attr('name')] = $(this).val(); // Add each to features object
                });
                User.savealert("allocatevehicle", data);
            }
        });
        $('#reportForm').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
                sweetAlert({
                    title: "!",
                    text: "select a valid date range",
                    confirmButtonText: "OK",
                    closeOnConfirm: true,
                });
            } else {
//e.preventDefault();

            }
        });
        $(document).on("click", ":button ", function() {
            User.ButtonClicked(this);
        });
        $.fn.modal.Constructor.prototype.enforceFocus = function() {
        };
        return User;
    })();
}).call(this);





