var Lock = function() {
    return {
        //main function to initiate the module
        init: function() {
            $.backstretch([
                "../assets/images/bg/1.jpg",
                "../assets/images/bg/2.jpg",
                "../assets/images/bg/3.jpg",
                "../assets/images/bg/4.jpg"
            ], {
                fade: 1000,
                duration: 8000
            });
            if ($.parseJSON($.cookie("creds"))===null){
               console.log($.parseJSON($.cookie("creds")));
               window.location.href="login.html";
            }

            $("#headName").html($.parseJSON($.cookie("creds")).name);
            $("#email").html($.parseJSON($.cookie("creds")).email);
            $("#fooName").html("Not " + $.parseJSON($.cookie("creds")).name + "?");
            $(document).on("click", ":button", function() {
                $.post("../LoginController", {email: $("#email").html(),password:$("#pasword").val(), action: "login"}, function(data) {
                    if (data.status == true) {
                        $.cookie("creds", JSON.stringify(data));
                        if (data.level == "admin") {
                            window.location.href = "admin.html";
                        } else if (data.level == "driver") {
                            window.location.href = "driver.html";
                        } else if (data.level == "agent") {
                            window.location.href = "agent.html";
                        } else {
                            $("#login-fail-div").html("Wrong password");
                        }
                    } else if (data.status === false) {
                        $("#login-fail-div").html("Wrong password");
                    }

                });

            });

        }

    };
}();
