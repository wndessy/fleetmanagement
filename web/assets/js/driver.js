//datepicker 

(function() {
    this.Driver = (function() {
        Driver = function() {
        };
        Driver.delteallert = function(element, item, attribute) {
            sweetAlert({
                title: "Are you sure of delete?",
                text: "You will not be able to recover this data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "red",
                confirmButtonText: "Yes, delete!",
                cancelButtonColor: "green",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.post("../EntityController", {item: item, action: "delete", data: $(element).attr(attribute)}, function(data) {
                        if (data === "logout") {
                            window.location.href = "lockPage.html";
                        } else {
                            sweetAlert({
                                title: "Sucess",
                                text: data,
                                type: "success",
                                timer: 1000,
                                showConfirmButton: false},
                            function(isConfirm) {
                                if (isConfirm) {
                                    window.location.href = window.location.href;
                                }
                            });
                        }
                    });
                } else {
                    sweetAlert({
                        title: "Cancelled",
                        text: "Delete action Cancelled ",
                        type: "error",
                        timer: 1000,
                        showConfirmButton: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
            });
        }

        Driver.savealert = function(item, data) {
            $.post("../EntityController", {item: item, action: "add", data: JSON.stringify(data)}, function(data) {
                if (data === "logout") {
                    window.location.href = "lockPage.html";
                } else {
                    swal({
                        title: "Success!",
                        text: "New " + item + " added successfully",
                        type: "success",
                        // timer: 1000,
                        showConfirmButton: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            if (item == "allocatevehicle") {
                                var win = window.open('../html/voucher.html?data=' + JSON.stringify(data), '_blank');
                                if (win) {
                                    //Browser has allowed it to be opened
                                    //  win.focus();
                                } else {
                                    //Broswer has blocked it
                                    alert('Please allow popups for this site');
                                }
                            } else {
                                window.location.href = window.location.href
                            }
                        }
                    });
                }
            });
        };
        /**
         * Datatables
         */
        $.post("../EntityController", {item: "DriverAssignment", action: "select"}, function(data) {
            if (data === "logout") {
                window.location.href = "lockPage.html";
            } else {
                var table = $('#assignments_table').dataTable({
                    responsive: true,
                    data: data,
                    "columns": [
                        {"data": "clientName", "title": "Client Name "},
                        {"data": "clientPhone", title: "Client Contact"},
                        {"data": "flightdetail", title: "Flight Detail"},
                        {"data": "pickupPlace", title: "PickupPlace"},
                        {"data": "pickupTime", title: "Pickup Time"},
                        {"data": "dropOffPlace", title: "DropOfPlace"},
                        {"data": "noOfTravellers", title: "No.Of Passengers"},
                        {"data": "regNo", title: "Vehicle"},
                        {"data": "model", title: "Model"}


                    ]
                });
            }
        }).fail(function() {
            alert("System error");
        });
        $.post("../EntityController", {item: "Driver", action: "select", type: ""}, function(data) {
            if (data === "logout") {
                window.location.href = "lockPage.html";
            } else {
                $('#driver_table').dataTable({
                    "dom": 'Rlfrtip',
                    data: data,
                    "columns": [
                        {"data": "name", title: "Name"},
                        {"data": "idNo", title: "Id or Passport"},
                        {"data": "email", title: "Email"},
                        {"data": "phone", title: "Phone Number"},
                    ]
                });
            }
        });
        
      
        //vehicle
        $.post("../EntityController", {item: "Vehicle", action: "select"}, function(data) {
            if (data === "logout") {
                window.location.href = "lockPage.html";
            } else {
                var table = $('#vehicle_table').DataTable({
                    //responsive: true,
                    "data": data,
                    "columns": [
                        {"data": "vehicleId", title: "", visible: false},
                        {"data": "regNo", "title": "Registration "},
                        {"data": "model", title: "Model"},
                        {"data": "color", title: "Color"},
                        {"data": "engine", title: "Engine capacity"},
                        {"data": "features", title: "Other featues"},
                        {"data": "assigned", title: "Status"},
                        {"data": "cancel", title: ""}
                    ],
                });
            
            }
        }).fail(function() {
            cnsole.log("error");
        });

        Driver.ButtonClicked = function(element) {


        }

        $('#client_form').validator().on('submit', function(e) {
            if (e.isDefaultPrevented()) {
//alert("issues");
            } else {
                var data = {};
                $("#client_form").not("button").find("input,select").each(function() {
                    data[$(this).attr('name')] = $(this).val(); // Add each to features object
                });
                Driver.savealert("Client", data);
                e.preventDefault();
            }
        });
        $(document).on("click", ":button ", function() {
            Driver.ButtonClicked(this);
        });
        $.fn.modal.Constructor.prototype.enforceFocus = function() {
        };
        return Driver;
    })();
}).call(this);





