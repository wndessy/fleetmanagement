package com.fleetManagement.utilities;

import com.fleetManagement.models.UserSessionsManager;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class loginStatusManager {

    public loginStatusManager(SessionFactory factory) {
        this.session = factory.openSession();
    }
    Session session = null;

    public boolean loginStatus(int sessionId) {
        boolean status = false;
        session.beginTransaction();
        Criteria criteriaDefaultData = session.createCriteria(UserSessionsManager.class);
        criteriaDefaultData.add(Restrictions.eq("sessionId", sessionId));
        criteriaDefaultData.add(Restrictions.eq("status", "active"));
        session.beginTransaction().commit();
        status = !criteriaDefaultData.list().isEmpty();
        session.close();
        return status;//true means logged in
    }
}
