package com.fleetManagement.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class dbConnect {

    private static final String url = "jdbc:postgresql://127.0.0.1:5432/fleetmanagement";
    private static final String user = "postgres";
    private static final String pasword = "w1nn1e";

public Connection connect() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, pasword);
            if (connection != null) {
            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException ex) {
            Logger.getLogger(dbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

}
