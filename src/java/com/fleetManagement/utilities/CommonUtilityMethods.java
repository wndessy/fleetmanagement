package com.fleetManagement.utilities;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommonUtilityMethods {

    public Timestamp StringToTimeStamp(String dateStr) {
        Timestamp timestamp = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd H:m:s");
            Date parsedDate = dateFormat.parse(dateStr);
            timestamp = new java.sql.Timestamp(parsedDate.getTime());
            System.out.println(timestamp);
        } catch (Exception e) {//this generic but you can control another types of exception
            e.printStackTrace();
        }
        return timestamp;
    }
//"05/29/2015 3:35 PM"dd-MMM-yy hh.mm.ss.S aa

    public static Timestamp StringToTimeStampBasic(String dateStr) {
        Timestamp timestamp = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm aa");
        Date parsedDate;
        try {
            parsedDate = dateFormat.parse(dateStr);
            timestamp = new Timestamp(parsedDate.getTime());
            System.out.println(timestamp);
        } catch (ParseException ex) {
            Logger.getLogger(CommonUtilityMethods.class.getName()).log(Level.SEVERE, null, ex);
        }
        return timestamp;
    }

    public String generateSalt() {
        return "";
    }

    public String GenerateHash(String Salt) {
        return "";
    }

    
    public static enum Mode {   

        ALPHA, ALPHANUMERIC, NUMERIC
    }

    public String generateRandomString(int length, Mode mode) throws Exception {

        StringBuffer buffer = new StringBuffer();
        String characters = "";

        switch (mode) {

            case ALPHA:
                characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                break;

            case ALPHANUMERIC:
                characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                break;

            case NUMERIC:
                characters = "1234567890";
                break;
        }

        int charactersLength = characters.length();

        for (int i = 0; i < length; i++) {
            double index = Math.random() * charactersLength;
            buffer.append(characters.charAt((int) index));
        }
        return buffer.toString();
    }

}
