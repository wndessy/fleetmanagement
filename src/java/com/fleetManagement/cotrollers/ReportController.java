
package com.fleetManagement.cotrollers;

import com.fleetManagement.models.VehicleAllocation;
import com.fleetManagement.utilities.CommonUtilityMethods;
import com.fleetManagement.utilities.dbConnect;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

public class ReportController extends HttpServlet {

    private static SessionFactory factory;

    static {
        factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Session session = null;

        session = factory.openSession();
        HttpSession httpSession = request.getSession(false);
        int orgid = 0;
        if (httpSession == null) {
            PrintWriter out = response.getWriter();
            out.print("logout");

        } else {
            orgid = Integer.parseInt(httpSession.getAttribute("orgId").toString());
        }

        try {

            String action = request.getParameter("action");
            String item = request.getParameter("item");
            String data = request.getParameter("data");

            //do not remove
            data = URLDecoder.decode(data, "UTF-8");
            data = data.replace("\"\"}", "\"}");
            JSONObject dataObj = new JSONObject(data);
            CommonUtilityMethods utility = new CommonUtilityMethods();
            // out.print(utility.StringToTimeStamp(startDate));
            if (action.equals("download_report")) {
                String startDate = dataObj.getString("startDate");
                String endDate = dataObj.getString("endDate");
                Connection connection = new dbConnect().connect();
                String commmon_sql = null;
                if (item.equals("vehicle_allocation")) {
                    commmon_sql = "select * from userentitys vehicles natural join vehicleallocation  natural join clients  where pickuptimes  between " + startDate + " and " + endDate + "";
                } else if (item.equals("clients")) {

                } else if (item.equals("vehicles")) {

                } else if (item.equals("drivers")) {

                }

                /*Map parametersMap = new HashMap();
                 parametersMap.put("startDate", utility.StringToTimeStamp(endDate));
                 parametersMap.put("endDate", utility.StringToTimeStamp(startDate));
                 JasperDesign jasperDesign = JRXmlLoader.load("/home/wndessy/NetBeansProjects/FleetManagement/Reports/vehicle - allocation.jrxml");
                 //String sql = "select * from userentitys natural join clients ";
                 //String sql1 = "select * from userentitys vehicles natural join vehicleallocation  natural join clients  where pickuptimes  between " + startDate + " and " + endDate + "";

                 JRDesignQuery qdesign = new JRDesignQuery();
                 qdesign.setText(commmon_sql);
                 jasperDesign.setQuery(qdesign);
                 JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                 //direct download
                 byte[] byteStream = JasperRunManager.runReportToPdf(jasperReport, null, connection);
                 response.setHeader("Content-Disposition", "inline, filename=myReport.pdf");
                 response.setContentType("application/pdf");
                 response.setContentLength(byteStream.length);
                 //outStream.write(byteStream, 0, byteStream.length);
                 //using jasper viewer
                 //                jasperReport = JasperCompileManager.compileReport(jasperDesign);
                 //                JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parametersMap, connection);
                 //                JasperViewer.viewReport(jasperPrint);
                 //                outStream.close();
                 //                connection.close();
                 out.close();*/
            } else if (action.equals("preview-report")) {
                String startDate = dataObj.getString("startDate");
                String endDate = dataObj.getString("endDate");
                response.setContentType("application/json;charste=\"utf-8\"");

                session.beginTransaction();
                Criteria criteria = session.createCriteria(VehicleAllocation.class);
                criteria.add(Restrictions.not(Restrictions.eq("status", "deleted")));

                // criteria.add(Restrictions.eq("org.orgID", orgid));
                String expectedPattern = "dd-MM-yyyy";
                SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
                Timestamp endTime_plus_one = new Timestamp(formatter.parse(endDate).getTime());
                Calendar cal = Calendar.getInstance();
                cal.setTime(endTime_plus_one);
                cal.add(Calendar.DAY_OF_WEEK, 1);
                endTime_plus_one.setTime(cal.getTime().getTime());
                criteria.add(Restrictions.between("allocationtTime", new Timestamp(formatter.parse(startDate).getTime()), endTime_plus_one));
                List<VehicleAllocation> allocationList = (List<VehicleAllocation>) criteria.list();
                session.beginTransaction().commit();
                session.close();
                JSONArray array = new JSONArray();
                if (!allocationList.isEmpty()) {
                    for (VehicleAllocation anItem : allocationList) {
                        JSONObject object = new JSONObject();
                        //client specific
                        object.put("Name", anItem.getClient().getName());
                        object.put("location", anItem.getClient().getLocation());
                        object.put("email", anItem.getClient().getEmail());
                        object.put("phone", anItem.getClient().getPhonenumber());
                        object.put("clientStatus", anItem.getClient().getStatus());
                        //driver based
                        object.put("driverName", anItem.getDriver().getName());
                        object.put("driverPhone", anItem.getDriver().getPhonenumber());
                        object.put("driverId", anItem.getDriver().getIdNumber());
                        object.put("driverStatus", anItem.getDriver().getStatus());
                        object.put("driverEmail", anItem.getDriver().getEmail());
                        //vehicle based
                        object.put("vehicleColor", anItem.getVehicle().getColour());
                        object.put("vehicleEngie", anItem.getVehicle().getEngine());
                        object.put("vehicleModel", anItem.getVehicle().getModel());
                        object.put("vehicleRegNo", anItem.getVehicle().getRegNo());
                        object.put("vehicleStatus", anItem.getVehicle().getStatus());
                        //agent based
                        object.put("agentName", anItem.getAgent().getName());
                        object.put("agentPhone", anItem.getAgent().getPhonenumber());
                        object.put("agentId", anItem.getAgent().getIdNumber());
                        object.put("agentStatus", anItem.getAgent().getStatus());
                        object.put("agentEmail", anItem.getAgent().getEmail());

                         //allication based
                        object.put("pickuptime", anItem.getPickupTimes());
                        object.put("clientsName", anItem.getClient().getName());
                        object.put("pickpPlace", anItem.getPickupPlace());
                        object.put("dropOffPlace", anItem.getDropOffPlace());
                        object.put("flightdetails", anItem.getFlightDetails());
                        object.put("allocationStatus", anItem.getStatus());
                        object.put("payment", anItem.getPayment());
                        array.add(object);
                    }
                }
                PrintWriter out = response.getWriter();
                out.print(array);
                out.close();

            } else if (action.equals("print_report")) {

                String url = dataObj.getString("url");
                String path = getServletContext().getRealPath("/assets/uploads/Report.pdf");
                //System.out.println(path);
                //String command = "wkhtmltopdf --page-size A4 --viewport-size 1280x1024 " + url + " " + path;
                String command = "wkhtmltopdf " + url + " " + path;
                try {
                    Runtime runTime = Runtime.getRuntime();
                    Process process = runTime.exec(command);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        System.err.println(line);
                    }
                    File f = new File(path);
                    //set the content type(can be excel/word/powerpoint etc..)
                    //get the file name
                    String name = f.getName().substring(f.getName().lastIndexOf("/") + 1, f.getName().length());
                    // modifies response
                    response.setHeader("Content-Disposition", "attachment;filename=\"" + name + "\"");
                    //set the header and also the Name by which user will be prompted to save
                    response.setContentType("application/pdf");
                    response.setContentLength((int) f.length());
                    //OPen an input stream to the file and post the file contents thru the 
                    //servlet output stream to the client m/c
                    InputStream in = new FileInputStream(f);
                    ServletOutputStream outs = response.getOutputStream();
                    int bit = 256;
                    while ((bit) >= 0) {
                        bit = in.read();
                        outs.write(bit);
                    }
                    //  factory.close();
                    outs.flush();
                    in.close();

                } catch (IOException exe) {
                    exe.printStackTrace();
                }
            }

        } catch (JSONException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
