package com.fleetManagement.cotrollers;

import com.fleetManagement.models.Agents;
import com.fleetManagement.models.Clients;
import com.fleetManagement.models.Drivers;
import com.fleetManagement.models.Organizations;
import com.fleetManagement.models.VehicleAllocation;
import com.fleetManagement.models.Vehicles;
import com.fleetManagement.utilities.CommonUtilityMethods;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

public class EntityController extends HttpServlet {

    private static SessionFactory factory;

    static {
        factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charste=\"utf-8\"");
        PrintWriter out = response.getWriter();
        CommonUtilityMethods utility = new CommonUtilityMethods();
        Session session = null;

        /**
         * default database data
         */
        session = factory.openSession();
        int orgid = 0;

        try {
            /**
             * data
             */

            HttpSession httpSession = request.getSession(false);
            if (httpSession == null) {
                out.print("logout");

            } else {
                orgid = Integer.parseInt(httpSession.getAttribute("orgId").toString());
                String item = request.getParameter("item");
                String action = request.getParameter("action");
                String data = request.getParameter("data");
                /**
                 * Entity
                 */
                if (item.equals("Client")) {
                    if (action.equals("select")) {
                        response.setContentType("application/json;charste=\"utf-8\"");
                        session = factory.openSession();
                        session.beginTransaction();
                        Criteria criteria = session.createCriteria(Clients.class);
                        criteria.add(Restrictions.eq("status", "active"))
                                .add(Restrictions.eq("org.orgID", orgid));
                        List<Clients> myentitys = (List<Clients>) criteria.list();

                        session.beginTransaction().commit();
                        JSONArray myarray = new JSONArray();
                        for (Clients client : myentitys) {
                            JSONObject obj = new JSONObject();
                            List<VehicleAllocation> alocation = (List<VehicleAllocation>) client.getAlocation();

                            obj.put("clientType", client.getType() == null ? "" : client.getType());
                            obj.put("phone", client.getPhonenumber() == null ? "" : client.getPhonenumber());
                            obj.put("name", client.getName() == null ? "" : client.getName());
                            obj.put("email", client.getEmail() == null ? "" : client.getEmail());
                            obj.put("location", client.getLocation() == null ? "" : client.getLocation());
                            obj.put("userEnityId", client.getUserentityId());
                            obj.put("status", " <p id=" + client.getUserentityId() + "'>" + ((alocation.size() > 0) ? "assigned" : "not assigned") + "</p>");
                            obj.put("assign", "<button type='button'name='assignVehicleBtn' class='btn btn-success btn-xs' data-id='" + client.getUserentityId() + "'>assign" + "</button>");
                            obj.put("cancel", "<button type='button' name='deleteClientBtn' class='btn btn-danger btn-xs'data-id='" + client.getUserentityId() + "'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");

                            myarray.add(obj);
                        }
                        out.println(myarray);
                        out.close();
                        session.close();

                    } else if (action.equals("add")) {
                        if (!data.equals("")) {
                            session = factory.openSession();
                            session.beginTransaction();
                            JSONObject dataObj = new JSONObject(data);
                            Clients client = new Clients();
                            Organizations org = new Organizations();
                            org.setOrgID(orgid);
                            client.setOrg(org);
                            client.setName(dataObj.getString("name"));
                            client.setEmail(dataObj.getString("email"));
                            client.setPhonenumber(dataObj.getString("phone"));
                            client.setEntityUserLevel("client");
                            client.setLocation(dataObj.getString("location"));
                            client.setPassword(utility.generateRandomString(10, CommonUtilityMethods.Mode.ALPHA));
                            client.setType(dataObj.getString("type"));
                            session.save(client);
                            session.beginTransaction().commit();
                            session.close();
                            out.print("Succcessfully added");
                        }
                    } else if (action.equals("update")) {
                    } else if (action.endsWith("delete")) {
                        session = factory.openSession();
                        session.beginTransaction();

                        int clientId = Integer.parseInt(data);
                        Query query = session.createSQLQuery("update UserEntitys set status = :status" + " where UserentityId= :clientId");
                        query.setParameter("status", "deleted");
                        query.setParameter("clientId", clientId);
                        int result = query.executeUpdate();

                        session.beginTransaction().commit();
                        session.close();
                        out.print("Succcessfully deleted");
                    }
                }
                /**
                 * driver
                 */
                if (item.equals("Driver")) {
                    if (action.equals("select")) {
                        response.setContentType("application/json;charste=\"utf-8\"");
                        session = factory.openSession();
                        session.beginTransaction();
                        Criteria criteria = session.createCriteria(Drivers.class);
                        criteria.add(Restrictions.eq("status", "active"))
                                .add(Restrictions.eq("org.orgID", orgid));
                        List<Drivers> myDrivers = (List<Drivers>) criteria.list();
                        session.beginTransaction().commit();
                        session.close();
                        JSONArray myarray = new JSONArray();
                        for (Drivers driver : myDrivers) {
                            JSONObject obj = new JSONObject();
                            obj.put("name", driver.getName() == null ? "" : driver.getName());
                            obj.put("idNo", driver.getIdNumber() == null ? "" : driver.getIdNumber());
                            obj.put("phone", driver.getPhonenumber() == null ? "" : driver.getPhonenumber());
                            obj.put("email", driver.getEmail() == null ? "" : driver.getEmail());
                            obj.put("userEnityId", driver.getUserentityId());
                            //Sobj.put("assigned", "<button type='button'  data-id='" + driver.getUserentityId() + "'>assignd</button>");
                            obj.put("cancel", "<button type='button' name='deleteDriverBtn' class='btn btn-danger btn-xs' data-id='" + driver.getUserentityId() + "'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");
                            myarray.add(obj);
                        }
                        out.println(myarray);
                        out.close();
                    } else if (action.equals("add")) {
                        if (!data.equals("")) {
                            session = factory.openSession();
                            session.beginTransaction();
                            JSONObject dataObj = new JSONObject(data);
                            Drivers driver = new Drivers();
                            Organizations org = new Organizations();
                            org.setOrgID(orgid);
                            driver.setOrg(org);
                            driver.setName(dataObj.getString("name"));
                            driver.setIdNumber(dataObj.getString("id"));
                            driver.setEmail(dataObj.getString("email"));
                            driver.setPhonenumber(dataObj.getString("phone"));
                            driver.setEntityUserLevel("driver");
                            driver.setPassword(utility.generateRandomString(10, CommonUtilityMethods.Mode.ALPHA));
                            session.save(driver);
                            session.beginTransaction().commit();
                            session.close();
                            out.print("Succcessfully added");
                        }
                    } else if (action.equals("update")) {
                    } else if (action.endsWith("delete")) {
                        session = factory.openSession();
                        session.beginTransaction();
                        int driverId = Integer.parseInt(data);
                        Query query = session.createSQLQuery("update UserEntitys set status = :status" + " where UserentityId= :driverId");
                        query.setParameter("status", "deleted");
                        query.setParameter("driverId", driverId);
                        int result = query.executeUpdate();

                        session.beginTransaction().commit();
                        session.close();
                        System.out.println("DLELETED");
                        out.print("Succcessfully deleted");
                    }
                } /**
                 * vehicle
                 */
                else if (item.equals("Vehicle")) {
                    if (action.equals("select")) {
                        response.setContentType("application/json;charste=\"utf-8\"");
                        session = factory.openSession();
                        session.beginTransaction();
                        Criteria criteria = session.createCriteria(Vehicles.class);
                        criteria.add(Restrictions.ilike("status", "active"))
                                .add(Restrictions.eq("org.orgID", orgid));
                        List<Vehicles> myVehicles = (List<Vehicles>) criteria.list();
                        session.beginTransaction().commit();
                        JSONArray myarray = new JSONArray();
                        for (Vehicles vehicle : myVehicles) {
                            JSONObject obj = new JSONObject();
                            obj.put("regNo", vehicle.getRegNo() == null ? "" : vehicle.getRegNo());
                            obj.put("model", vehicle.getModel() == null ? "" : vehicle.getModel());
                            obj.put("color", vehicle.getColour() == null ? "" : vehicle.getColour());
                            obj.put("engine", vehicle.getEngine() == null ? "" : vehicle.getEngine());
                            obj.put("features", vehicle.getVehicleFeatures() == null ? "" : vehicle.getVehicleFeatures());
                            obj.put("vehicleId", vehicle.getVehicleId());
                            obj.put("assigned", "<a data-id='" + vehicle.getVehicleId() + "'>assignd</a>");
                            obj.put("cancel", "<button type='button'  name='deleteVehicleBtn' class='btn btn-danger btn-xs' data-id='" + vehicle.getVehicleId() + "'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");
                            myarray.add(obj);
                        }
                        out.println(myarray);
                        out.close();
                        session.close();
                        //out.print("hellow");
                    } else if (action.equals("add")) {
                        if (!data.equals("")) {
                            session = factory.openSession();
                            session.beginTransaction();
                            JSONObject dataObj = new JSONObject(data);
                            Vehicles v = new Vehicles();
                            Organizations org = new Organizations();
                            org.setOrgID(orgid);
                            v.setColour(dataObj.getString("colour"));
                            v.setRegNo(dataObj.getString("regNo"));
                            v.setModel(dataObj.getString("model"));
                            v.setEngine(dataObj.getString("engine"));
                            v.setOrg(org);
                            session.save(v);

                            session.beginTransaction().commit();
                            session.close();

                            out.print("Succcessfully added");
                        }
                    } else if (action.equals("update")) {

                    } else if (action.endsWith("delete")) {
                        session = factory.openSession();
                        session.beginTransaction();
                        int vehicleId = Integer.parseInt(data);

                        Query query = session.createSQLQuery("update Vehicles set status = :status" + " where vehicleId= :vehicleId");
                        query.setParameter("status", "deleted");
                        query.setParameter("vehicleId", vehicleId);
                        int result = query.executeUpdate();

                        session.beginTransaction().commit();
                        session.close();
                        out.print("Succcessfully deleted");
                    }
                } else if (item.equals("Agent")) {
                    if (action.equals("select")) {
                        response.setContentType("application/json;charste=\"utf-8\"");
                        session = factory.openSession();
                        session.beginTransaction();
                        Criteria criteria = session.createCriteria(Agents.class);
                        criteria.add(Restrictions.eq("status", "active"))
                                .add(Restrictions.eq("org.orgID", orgid));
                        List<Agents> myentitys = (List<Agents>) criteria.list();
                        session.beginTransaction().commit();
                        JSONArray myarray = new JSONArray();
                        for (Agents agents : myentitys) {
                            JSONObject obj = new JSONObject();
                            List<VehicleAllocation> alocation = (List<VehicleAllocation>) agents.getAlocation();
                            obj.put("phone", agents.getPhonenumber() == null ? "" : agents.getPhonenumber());
                            obj.put("name", agents.getName() == null ? "" : agents.getName());
                            obj.put("email", agents.getEmail() == null ? "" : agents.getEmail());
                            obj.put("IdNo", agents.getIdNumber() == null ? "" : agents.getIdNumber());

                            obj.put("cancel", "<button type='button' name='deleteClientBtn'class='btn btn-danger btn-xs' data-id='" + agents.getUserentityId() + "'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");
                            myarray.add(obj);
                        }
                        out.println(myarray);
                        out.close();
                        session.close();

                    } else if (action.equals("add")) {
                        if (!data.equals("")) {
                            session = factory.openSession();
                            session.beginTransaction();
                            JSONObject dataObj = new JSONObject(data);

                            Agents agents = new Agents();
                            Organizations org = new Organizations();
                            org.setOrgID(orgid);
                            agents.setOrg(org);
                            agents.setName(dataObj.getString("name"));
                            agents.setEmail(dataObj.getString("email"));
                            agents.setPhonenumber(dataObj.getString("phone"));
                            agents.setEntityUserLevel("agent");
                            agents.setPassword(utility.generateRandomString(10, CommonUtilityMethods.Mode.ALPHA));
                            session.save(agents);
                            session.beginTransaction().commit();
                            session.close();
                            out.print("Succcessfully added");
                        }
                    } else if (action.equals("update")) {
                    } else if (action.endsWith("delete")) {
                        session = factory.openSession();
                        session.beginTransaction();
                        int agentId = Integer.parseInt(data);
                        Query query = session.createSQLQuery("update UserEntitys set status = :status" + " where UserentityId= :agentId");
                        query.setParameter("status", "deleted");
                        query.setParameter("agentId", agentId);
                        int result = query.executeUpdate();
                        session.beginTransaction().commit();
                        session.close();
                        out.print("Succcessfully deleted");
                    }
                } else if (item.equals("allocatevehicle")) {
                    response.setContentType("application/json;charste=\"utf-8\"");
                    session = factory.openSession();
                    session.beginTransaction();
                    JSONObject dataObj = new JSONObject(data);
                    Organizations org = new Organizations();
                    org.setOrgID(orgid);
                    Vehicles vehicle = new Vehicles();
                    vehicle.setVehicleId(Integer.parseInt(dataObj.getString("vehicle-select")));
                    Drivers driver = new Drivers();
                    driver.setUserentityId(Integer.parseInt(dataObj.getString("driver-select")));
                    Clients client = new Clients();
                    client.setUserentityId(Integer.parseInt(dataObj.getString("userId")));
                    Agents agents = new Agents();
                    agents.setUserentityId(Integer.parseInt(httpSession.getAttribute("userId").toString()));
                    VehicleAllocation allocate = new VehicleAllocation();

                    allocate.setClient(client);
                    allocate.setDriver(driver);
                    allocate.setVehicle(vehicle);
                    allocate.setOrg(org);
                    allocate.setAgent(agents);
                    allocate.setPickupPlace(dataObj.getString("Pickup-Place"));
                    allocate.setDropOffPlace(dataObj.getString("dropoff-place"));
                    allocate.setPickupTimes(utility.StringToTimeStampBasic(dataObj.getString("Pickup-Time")));
                    allocate.setPayment(dataObj.getInt("payment"));
                    allocate.setNoOftravellers(new Integer(dataObj.getString("noOfTravellers")));
                    allocate.setFlightDetails(dataObj.getString("flightDetails"));
                    session.save(allocate);

                    //get the specific client details
                    Criteria criteria = session.createCriteria(Clients.class);
                    criteria.add(Restrictions.eq("userentityId", Integer.parseInt(dataObj.getString("userId"))));
                    Clients myclient = (Clients) criteria.list().get(0);
                    //get specific driver details
                    Criteria criteriaDriver = session.createCriteria(Drivers.class);
                    criteriaDriver.add(Restrictions.eq("userentityId", Integer.parseInt(dataObj.getString("driver-select"))));
                    Drivers myDriver = (Drivers) criteriaDriver.list().get(0);
                    //get specific vehicle details
                    Criteria criteriaVehicle = session.createCriteria(Vehicles.class);
                    criteriaVehicle.add(Restrictions.eq("vehicleId", Integer.parseInt(dataObj.getString("vehicle-select"))));
                    Vehicles myVehicle = (Vehicles) criteriaVehicle.list().get(0);

                    session.beginTransaction().commit();
                    session.close();

                    JSONObject object = new JSONObject();
                    object.put("clientName", myclient.getName());
                    object.put("clientPhoneNumber", myclient.getPhonenumber());
                    object.put("vehicle", myVehicle.getModel() + " " + myVehicle.getRegNo());
                    object.put("pickuptime", dataObj.getString("Pickup-Time"));
                    object.put("pickupplace", allocate.getPickupPlace());
                    object.put("dropOffplace", allocate.getDropOffPlace());
                    object.put("noOfTravellers", allocate.getNoOftravellers());
                    object.put("flightDetails", allocate.getFlightDetails());
                    object.put("driverName", myDriver.getName());
                    object.put("driverPhoneNumber", myDriver.getPhonenumber());
                    object.put("organization", "");
                    object.put("KMSIN", "");
                    object.put("KMSOUT", "");
                    object.put("KMSTOTAL", "");

                    out.print(object);

                } else if (item.equals("selectallocatedVehiclePerClient")) {
                    response.setContentType("application/json;charste=\"utf-8\"");

                    session = factory.openSession();
                    session.beginTransaction();
                    int clientId = Integer.parseInt(data);
                    Query query = session.createSQLQuery("update VehicleAllocation set status = :status where clientId= :agentId and pickupTimes < now()");
                    query.setParameter("status", "done");
                    query.setParameter("agentId", clientId);
                    int result = query.executeUpdate();

                    Criteria criteria;
                    criteria = session.createCriteria(VehicleAllocation.class)
                            .createAlias("org", "org")
                            .createAlias("client", "client")
                            .add(Restrictions.ilike("status", "active"))
                            .add(Restrictions.eq("org.orgID", orgid))
                            .add(Restrictions.eq("client.userentityId", clientId));
                    List<VehicleAllocation> list = (List<VehicleAllocation>) criteria.list();
                    session.beginTransaction().commit();
                    session.close();
                    JSONArray array = new JSONArray();
                    for (VehicleAllocation anItem : list) {
                        JSONObject object = new JSONObject();
                        object.put("vehicle", anItem.getVehicle().getRegNo() + "" + anItem.getVehicle().getModel());
                        object.put("driver", anItem.getDriver().getName());
                        object.put("pickuptime", anItem.getPickupTimes());
                        object.put("pickupplace", anItem.getPickupPlace());
                        object.put("dropOffplace", anItem.getDropOffPlace());
                        object.put("cancel", "<button type='button' name='cancelVehicleAllocationBtn' class='btn btn-danger btn-xs' class='glyphicon glyphicon-remove btn-danger' data-id='cancelAlloc" + anItem.getAllocationId() + "'></button>");
                        array.add(object);
                    }
                    out.print(array);
                } else if (item.equals("cancelVehicleAllocation")) {
                    session = factory.openSession();
                    session.beginTransaction();
                    VehicleAllocation alocation = new VehicleAllocation();
                    int alocid = Integer.parseInt(data);
                    alocation.setAllocationId(alocid);
                    session.delete(alocation);
                    session.beginTransaction().commit();
                    session.close();
                    out.print("Succcessfully deleted");
                } /**
                 *
                 * for Driver Page
                 */
                else if (item.equals("DriverAssignment")) {
                    response.setContentType("application/json;charste=\"utf-8\"");
                    session = factory.openSession();
                    session.beginTransaction();
                    Criteria criteria = session.createCriteria(VehicleAllocation.class);
                    criteria.add(Restrictions.ilike("status", "active"))
                            .add(Restrictions.eq("org.orgID", orgid));

                    String oneOfManyDriverId = request.getParameter("oneOfManyDriverId");
                    String oneOfManyVehicleId = request.getParameter("oneOfManyVehicleId");
                    String oneOfManyClientId = request.getParameter("oneOfManyClientId");

                    if (!(oneOfManyDriverId==null)) {
                        criteria.add(Restrictions.eq("driver.userentityId", Integer.parseInt(oneOfManyDriverId)));
                    } else if (!(oneOfManyVehicleId == null)) {
                        criteria.add(Restrictions.eq("vehicle.vehicleId", Integer.parseInt(oneOfManyVehicleId)));
                    } else if (!(oneOfManyClientId == null)) {
                        criteria.add(Restrictions.eq("client.userentityId", Integer.parseInt(oneOfManyClientId)));
                    } else {
                        criteria.add(Restrictions.eq("driver.userentityId", Integer.parseInt(httpSession.getAttribute("userId").toString())));
                    }

                    List<VehicleAllocation> myVehicleAllocation = (List<VehicleAllocation>) criteria.list();
                    session.beginTransaction().commit();
                    JSONArray myarray = new JSONArray();
                    for (VehicleAllocation vehicleAllocation : myVehicleAllocation) {

                        System.out.println("==============="+vehicleAllocation.getClient().getName());

                        JSONObject obj = new JSONObject();
                        obj.put("regNo", vehicleAllocation.getVehicle().getRegNo() == null ? "" : vehicleAllocation.getVehicle().getRegNo());
                        obj.put("model", vehicleAllocation.getVehicle().getModel() == null ? "" : vehicleAllocation.getVehicle().getModel());
                        obj.put("color", vehicleAllocation.getVehicle().getColour() == null ? "" : vehicleAllocation.getVehicle().getColour());
                        obj.put("clientName", vehicleAllocation.getClient().getName() == null ? "" : vehicleAllocation.getClient().getName());
                        obj.put("clientPhone", vehicleAllocation.getClient().getPhonenumber() == null ? "" : vehicleAllocation.getClient().getPhonenumber());
                        obj.put("clientLocation", vehicleAllocation.getClient().getLocation() == null ? "" : vehicleAllocation.getClient().getLocation());
                        obj.put("pickupPlace", vehicleAllocation.getPickupPlace() == null ? "" : vehicleAllocation.getPickupPlace());
                        obj.put("pickupTime", vehicleAllocation.getPickupTimes() == null ? "" : vehicleAllocation.getPickupTimes());
                        obj.put("dropOffPlace", vehicleAllocation.getDropOffPlace() == null ? "" : vehicleAllocation.getDropOffPlace());
                        obj.put("flightdetail", vehicleAllocation.getFlightDetails() == null ? "" : vehicleAllocation.getFlightDetails());
                        obj.put("driverName", vehicleAllocation.getDriver().getName());

                        obj.put("noOfTravellers", vehicleAllocation.getNoOftravellers());
                        obj.put("cancel", "<button type='button' name='cancelVehicleAllocationBtn' class='btn btn-danger btn-xs' data-id='" + vehicleAllocation.getAllocationId() + "'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");

                        myarray.add(obj);
                    }
                    out.println(myarray);
                    out.close();
                    session.close();

                }

                //
                /// criteria.setProjection(Projections.max("name"));
                // .add(Restrictions.or(Restrictions.eq("me", "me"), Restrictions.eq("username", "mee")));
                //			UserEntitys myent = new UserEntitys();
                //			myent.setIdNumber("");
                //			Example example= Example.create(myent).excludeProperty("name");
                //			criteria1.add(example);
                //ignores null properties and primary key
                //cacheing
                /**
                 * comment
                 */
                // factory.close();
                out.close();
            }
        } catch (JSONException ex) {
            Logger.getLogger(EntityController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(EntityController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
