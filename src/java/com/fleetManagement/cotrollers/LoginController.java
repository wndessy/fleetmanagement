package com.fleetManagement.cotrollers;

import com.fleetManagement.models.Organizations;
import com.fleetManagement.models.UserEntitys;
import com.fleetManagement.models.UserSessionsManager;
import com.fleetManagement.utilities.CommonUtilityMethods;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.json.simple.JSONObject;

public class LoginController extends HttpServlet {

    private static SessionFactory factory;

    static {
        factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charste=\"utf-8\"");
        PrintWriter out = response.getWriter();
        CommonUtilityMethods utility = new CommonUtilityMethods();

        Session session = null;

        /**
         * default database data
         */
        session = factory.openSession();
        session.beginTransaction();
        Criteria criteriaDefaultData = session.createCriteria(Organizations.class);
        criteriaDefaultData.add(Restrictions.eq("orgName", "Rickshaw travel limited"));
        List<Organizations> listorgs = (List<Organizations>) criteriaDefaultData.list();
        session.beginTransaction().commit();
        session.close();
        int orgid = 0;
        if (listorgs.isEmpty()) {
            out.println("its empty");
            session = factory.openSession();
            session.beginTransaction();
            Organizations org = new Organizations();
            org.setOrgName("Rickshaw travel limited");
            session.save(org);
            session.beginTransaction().commit();
            session.close();

            session = factory.openSession();
            session.beginTransaction();
            UserEntitys userEntitys = new UserEntitys();
            userEntitys.setOrg(org);
            userEntitys.setEntityUserLevel("admin");
            userEntitys.setName("admin");//to be gotten fromxml
            userEntitys.setEmail("admin@rickshaw.com");
            userEntitys.setPassword("rickshaw");
            session.save(userEntitys);
            session.beginTransaction().commit();
            session.close();

            session = factory.openSession();
            session.beginTransaction();
            Criteria criteriaselectOrgId = session.createCriteria(Organizations.class);
            criteriaDefaultData.add(Restrictions.eq("orgName", "Rickshaw travel limited"));
            List<Organizations> listorg = (List<Organizations>) criteriaselectOrgId.list();
            session.beginTransaction().commit();
            session.close();
            orgid = listorg.get(0).getOrgID();
        } else {
            // out.println("The company is registered Already");
            orgid = listorgs.get(0).getOrgID();
        }

        session = factory.openSession();
        session.beginTransaction();
        String logout = request.getParameter("action");

        if (logout.equals("logout")) {
            HttpSession httpSession = request.getSession(false);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("action", logout);
            out.print(jSONObject);
            httpSession.invalidate();

        } else if (logout.equals("login")) {

            String email = request.getParameter("email").trim();
            String password = request.getParameter("password").trim();
            Criteria criterialogin = session.createCriteria(UserEntitys.class);
            criterialogin.add(Restrictions.eq("email", email))
              .add(Restrictions.eq("password",password)).
               add(Restrictions.not(Restrictions.eq("entityUserLevel", "client")));
              
            session.beginTransaction().commit();
            List<UserEntitys> entitys = criterialogin.list();
            session.close();
            JSONObject jSONObjectResult = new JSONObject();
            if (entitys.isEmpty()) {
                jSONObjectResult.put("status", false);
                jSONObjectResult.put("level", null);

            } else {
                HttpSession httpSession = request.getSession(true);
                httpSession.setAttribute("userId", entitys.get(0).getUserentityId());
                httpSession.setAttribute("userEmail", entitys.get(0).getEmail());
                httpSession.setAttribute("orgId", entitys.get(0).getOrg().getOrgID());
//                httpSession.setAttribute("userLevel", entitys.get(0).getEntityUserLevel());
//
//                int sessionTimeout = 600;//in seconds willget from xml
//
//                httpSession.setMaxInactiveInterval(sessionTimeout);
//                // httpSession.getCreationTime();
                jSONObjectResult.put("status", true);
                jSONObjectResult.put("level", entitys.get(0).getEntityUserLevel());
                jSONObjectResult.put("name", entitys.get(0).getName());
                jSONObjectResult.put("email", entitys.get(0).getEmail());
                

                session = factory.openSession();
                session.beginTransaction();
                UserSessionsManager userSession = new UserSessionsManager();
                userSession.setEntities(entitys.get(0));
                userSession.setStartTime(new Timestamp(new Date().getTime()));
              //  Timestamp endTime = new Timestamp((long) httpSession.getLastAccessedTime() + (sessionTimeout));
              //  userSession.setEndTimes(endTime);
                userSession.setSessionId(httpSession.getId());
                session.save(userSession);
                session.beginTransaction().commit();

            }
            out.println(jSONObjectResult);
        }
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
