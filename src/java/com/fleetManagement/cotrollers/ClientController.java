    package com.fleetManagement.cotrollers;

import com.fleetManagement.models.Clients;
import com.fleetManagement.models.Organizations;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;

public class ClientController extends HttpServlet {

    private static SessionFactory factory;

    static {
        factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Session session = null;
        String action = request.getParameter("action");
        String data = request.getParameter("data");
        try {

            if (action.equals("select")) {
                response.setContentType("application/json;charste=\"utf-8\"");
                session = factory.openSession();
                session.beginTransaction();
                Query myquery = session.createQuery("from Clients ");
                List<Clients> myentitys = (List<Clients>) myquery.list();
                session.beginTransaction().commit();

                JSONArray myarray = new JSONArray();
                for (Clients client : myentitys) {
                    JSONObject obj = new JSONObject();
                    obj.put("clientType", client.getType() == null ? "" : client.getType());
                    obj.put("phone", client.getPhonenumber() == null ? "" : client.getPhonenumber());
                    obj.put("name", client.getName() == null ? "" : client.getName());
                    obj.put("email", client.getEmail() == null ? "" : client.getEmail());
                    obj.put("id", client.getIdNumber() == null ? "" : client.getIdNumber());
                    obj.put("assigned", "<a data-id='" + client.getUserentityId() + "'>assignd</a>");
                    obj.put("cancel", "<button type='button' name='deleteClientBtn' data-id='" + client.getUserentityId() + "'><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>");

                    myarray.add(obj);
                }
                out.println(myarray);
                out.close();
                session.close();

            } else if (action.equals("add")) {
                if (!data.equals("")) {
                    session = factory.openSession();
                    session.beginTransaction();
                    JSONObject dataObj = new JSONObject(data);
                    Clients client = new Clients();
                    Organizations org = new Organizations();
                    org.setOrgID(8732);
                    client.setOrg(org);
                    client.setName(dataObj.getString("name"));
                    client.setIdNumber(dataObj.getString("id"));
                    client.setEmail(dataObj.getString("email"));
                    client.setPhonenumber(dataObj.getString("phone"));
                    client.setEntityUserLevel("2");
                    client.setPassword("123");
                    client.setType(dataObj.getString("type"));
                    session.save(client);
                    session.beginTransaction().commit();
                    session.close();
                    out.print("Succcessfully added");
                }
            } else if (action.equals("update")) {

            } else if (action.endsWith("delete")) {
//					session = factory.openSession();
//					session.beginTransaction();
//					Clients client = new Clients();
//					session.delete(client);
//					session.beginTransaction().commit();
//					session.close();
                out.print("Succcessfully deleted");
            }

        } catch (JSONException ex) {
            Logger.getLogger(EntityController.class.getName()).log(Level.SEVERE, null, ex);
        }
        out.close();

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>	

}
