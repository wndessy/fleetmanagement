package com.fleetManagement.models;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Agents extends UserEntitys implements Serializable {
	private String details;
	@ElementCollection
	@OneToMany(mappedBy = "agent", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	Collection<VehicleAllocation> alocation = new ArrayList<VehicleAllocation>();
	
        public String getDetails() {
		return details;
	}
        
	public void setDetails(String details) {
		this.details = details;
	}
        
	public Collection<VehicleAllocation> getAlocation() {
		return alocation;
	}
        
	public void setAlocation(Collection<VehicleAllocation> alocation){
		this.alocation = alocation;
	}

}
