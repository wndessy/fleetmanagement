package com.fleetManagement.models;

import java.sql.Timestamp;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UserSessionsManager {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String sessionId;
    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp startTime;
    private Timestamp endTimes;

   // @Column(columnDefinition = "DEFAULT active")
   // private String status;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "userentityId")
    private UserEntitys entities;

    public UserEntitys getEntities() {
        return entities;
    }

    public void setEntities(UserEntitys entities) {
        this.entities = entities;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Timestamp getEndTimes() {
        return endTimes;
    }

    public void setEndTimes(Timestamp endTimes) {
        this.endTimes = endTimes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
