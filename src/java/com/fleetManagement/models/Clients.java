package com.fleetManagement.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Clients extends UserEntitys implements Serializable {
    public Clients() {
    }
    private String type;
    private String location;
    private String details;
    @ElementCollection
    @OneToMany(mappedBy = "client", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
        Collection<VehicleAllocation> alocation = new ArrayList<VehicleAllocation>();

    public Collection<VehicleAllocation> getAlocation() {
        return alocation;
    }

    public void setAlocation(Collection<VehicleAllocation> alocation) {
        this.alocation = alocation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

}
