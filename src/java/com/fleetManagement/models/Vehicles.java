package com.fleetManagement.models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Vehicles implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int vehicleId;
    private String regNo;
    private String model;
    private String colour;
    private String engine;
    private String vehicleFeatures;

    @Column(insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp creationDate;
    @Column(updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp lastupdate;   
    @Column(insertable = false, columnDefinition = "varchar(20) DEFAULT 'active'")
    private String status;

    private String type;
    private String details;
    @ElementCollection
    @OneToMany(mappedBy = "vehicle", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    Collection<VehicleAllocation> alocation = new ArrayList<VehicleAllocation>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "groupId")
    private VehicleGroups group;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "orgId")
    private Organizations org;

    public Organizations getOrg() {
        return org;
    }

    public void setOrg(Organizations org) {
        this.org = org;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Collection<VehicleAllocation> getAlocation() {
        return alocation;
    }

    public void setAlocation(Collection<VehicleAllocation> alocation) {
        this.alocation = alocation;
    }

    public VehicleGroups getGroup() {
        return group;
    }

    public void setGroup(VehicleGroups group) {
        this.group = group;
    }

    public void setOrg(VehicleGroups group) {
        this.group = group;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getVehicleFeatures() {
        return vehicleFeatures;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setVehicleFeatures(String VehicleFeatures) {
        this.vehicleFeatures = VehicleFeatures;
    }

    public Date getLastupdate() {
        return lastupdate;
    }

    public void setLastupdate(Timestamp lastupdate) {
        this.lastupdate = lastupdate;
    }

}
