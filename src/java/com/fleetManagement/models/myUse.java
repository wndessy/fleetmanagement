package com.fleetManagement.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
public class myUse implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int sessionId;
    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp startTime;
    private Timestamp endTimes;
    
    @Column(columnDefinition = "DEFAULT active")
    private String status;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "userentityId")
    private UserEntitys entities;

    public UserEntitys getEntities() {
        return entities;
    }
    public void setEntities(UserEntitys entities) {
        this.entities = entities;
    }
    public Timestamp getStartTime() {
        return startTime;
    }
    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public Timestamp getEndTimes() {
        return endTimes;
    }

    public void setEndTimes(Timestamp endTimes) {
        this.endTimes = endTimes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
