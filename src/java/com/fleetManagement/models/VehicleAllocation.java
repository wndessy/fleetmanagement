package com.fleetManagement.models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Entity
public class VehicleAllocation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int allocationId;
    private Timestamp pickupTimes;
    private String pickupPlace;
    private String DropOffPlace;
    private String flightDetails;
    private int payment;
    private int noOftravellers;
    @Column(insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp allocationtTime;
    @Column(updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp lastUpdate;
    @Column(insertable = false, columnDefinition = "varchar(20) DEFAULT 'active'")
    private String status;

    @ManyToOne
    @JoinColumn(name = "orgId")
    private Organizations org;
    @ManyToOne
    @JoinColumn(name = "driverId")
    private Drivers driver;
    @ManyToOne
    @JoinColumn(name = "clientId")
    private Clients client;
    @ManyToOne
    @JoinColumn(name = "vehicleId")
    private Vehicles vehicle;

    @ManyToOne
    @JoinColumn(name = "agentId")
    private Agents agent;

    public Organizations getOrg() {
        return org;
    }

    public void setOrg(Organizations org) {
        this.org = org;
    }

    public int getAllocationId() {
        return allocationId;
    }

    public void setAllocationId(int allocationId) {
        this.allocationId = allocationId;
    }

    public Vehicles getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicles vehicle) {
        this.vehicle = vehicle;
    }

    public Timestamp getPickupTimes() {
        return pickupTimes;
    }

    public void setPickupTimes(Timestamp pickupTimes) {
        this.pickupTimes = pickupTimes;
    }

    public String getPickupPlace() {
        return pickupPlace;
    }

    public void setPickupPlace(String pickupPlace) {
        this.pickupPlace = pickupPlace;
    }

    public String getDropOffPlace() {
        return DropOffPlace;
    }

    public void setDropOffPlace(String DropOffPlace) {
        this.DropOffPlace = DropOffPlace;
    }

    public Timestamp getAllocationtTime() {
        return allocationtTime;
    }

    public void setAllocationtTime(Timestamp allocationtTime) {
        this.allocationtTime = allocationtTime;
    }

    public Drivers getDriver() {
        return driver;
    }

    public void setDriver(Drivers driver) {
        this.driver = driver;
    }

    public Clients getClient() {
        return client;
    }

    public void setClient(Clients client) {
        this.client = client;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    @PreUpdate
    @PrePersist
    public void setLastUpdate(Timestamp lastUpdate) {
        Date date = new java.util.Date();
        this.lastUpdate = new Timestamp(date.getTime());
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlightDetails() {
        return flightDetails;
    }

    public void setFlightDetails(String flightDetails) {
        this.flightDetails = flightDetails;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    public int getNoOftravellers() {
        return noOftravellers;
    }

    public void setNoOftravellers(int noOftravellers) {
        this.noOftravellers = noOftravellers;
    }

    public Agents getAgent() {
        return agent;
    }

    public void setAgent(Agents agent) {
        this.agent = agent;
    }

}
