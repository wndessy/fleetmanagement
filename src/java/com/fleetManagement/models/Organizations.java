package com.fleetManagement.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

//	@JoinTable(
//			joinColumns = @JoinColumn(name = "ordId"),
//			inverseJoinColumns = @JoinColumn(name = "vehicleId"))


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Organizations implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int orgID;
    private String orgName;
    @ElementCollection
    @OneToMany(mappedBy = "org", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    Collection<UserEntitys> clients = new ArrayList<UserEntitys>();
    @ElementCollection
    @OneToMany(mappedBy = "org", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    Collection<Vehicles> vehicles = new ArrayList<Vehicles>();
    @ElementCollection
    @OneToMany(mappedBy = "org", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    Collection<VehicleAllocation> allocations = new ArrayList<VehicleAllocation>();
  
    public Collection<VehicleAllocation> getAllocations() {
        return allocations;
    }

    public void setAllocations(Collection<VehicleAllocation> allocations) {
        this.allocations = allocations;
    }

    public Collection<Vehicles> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Collection<Vehicles> vehicles) {
        this.vehicles = vehicles;
    }

    public Collection<UserEntitys> getClients() {
        return clients;
    }

    public void setClients(Collection<UserEntitys> clients) {
        this.clients = clients;
    }

    public int getOrgID() {
        return orgID;
    }

    public void setOrgID(int orgID) {
        this.orgID = orgID;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

}
