
package com.fleetManagement.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import org.hibernate.annotations.ManyToAny;

@Entity
public class VehicleGroups implements Serializable {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private int groupId;
public String groupName;

@ElementCollection
@OneToMany(mappedBy = "group", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
public Collection<Vehicles>vehicle = new ArrayList<Vehicles>();
	public Collection<Vehicles> getVehicle() {
		return vehicle;
	}
	public void setVehicle(Collection<Vehicles> vehicle) {
		this.vehicle = vehicle;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	
}
