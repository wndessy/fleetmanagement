package com.fleetManagement.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

@Entity
public class Drivers extends UserEntitys implements Serializable {

	private String driverDetails;
	private String type;
	private String details;
        
	@ElementCollection
	@OneToMany(mappedBy = "driver", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	Collection<VehicleAllocation> alocation = new ArrayList<VehicleAllocation>();

	public String getType(){
		return type;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getDetails(){
		return details;
	}

	public void setDetails(String details){
		this.details = details;
	}

	public Collection<VehicleAllocation> getAlocation(){
		return alocation;
	}

	public void setAlocation(Collection<VehicleAllocation> alocation){
		this.alocation = alocation;
	}

	public String getDriverDetails(){
		return driverDetails;
	}

	public void setDriverDetails(String driverDetails){
		this.driverDetails = driverDetails;
	}

}
