package com.fleetManagement.models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
//@NamedQueries(name="UserEntitys.byId",query="from UserEntitys")
//@NamedNativeQueries(Name="",Query="",resultClass=ThisReference.)
public class UserEntitys implements Serializable {

    public UserEntitys() {
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userentityId;
    private String name;
    private String email;
    private String password;
    private String entityUserLevel;
    private String idNumber;
    private String phonenumber;
    private String salt;

    
    @Column(insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp creationDate;
    @Column(updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp lastUpdate;
    @Column(insertable = false, columnDefinition = "varchar(20) DEFAULT 'active'")
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "orgid")
    private Organizations org;

    @ElementCollection
    @OneToMany(mappedBy = "entities", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    public Collection<UserSessionsManager> session = new ArrayList<UserSessionsManager>();

    public Collection<UserSessionsManager> getSession() {
        return session;
    }
    public void setSession(Collection<UserSessionsManager> session) {
        this.session = session;
    }
    public Organizations getOrg() {
        return org;
    }

    public void setOrg(Organizations org) {
        this.org = org;
    }

    public int getUserentityId() {
        return userentityId;
    }

    public void setUserentityId(int userentityId) {
        this.userentityId = userentityId;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEntityUserLevel() {
        return entityUserLevel;
    }

    public void setEntityUserLevel(String entityUserLevel) {
        this.entityUserLevel = entityUserLevel;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
