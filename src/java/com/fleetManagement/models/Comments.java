package com.fleetManagement.models;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.CascadeType;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Comments extends UserEntitys implements Serializable {

    private String comment;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp comentDate;
    @Column(columnDefinition = "varchar(20) DEFAULT 'active'")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Timestamp getComentDate() {
        return comentDate;
    }

    public void setComentDate(Timestamp comentDate) {
        this.comentDate = comentDate;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
